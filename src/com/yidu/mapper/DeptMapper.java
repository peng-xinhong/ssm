package com.yidu.mapper;

import java.util.List;

import com.yidu.entity.Dept;

public interface DeptMapper {

	public List<Dept> queryDepts();
	
}
