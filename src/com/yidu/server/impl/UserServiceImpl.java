package com.yidu.server.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yidu.entity.Dept;
import com.yidu.mapper.DeptMapper;
import com.yidu.server.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	DeptMapper deptMapper;
	
	public List<Dept> queryDepts(){
		return deptMapper.queryDepts();
	}
	
}
