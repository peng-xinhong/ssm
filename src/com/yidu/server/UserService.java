package com.yidu.server;

import java.util.List;

import com.yidu.entity.Dept;

public interface UserService {

	public List<Dept> queryDepts();
}
