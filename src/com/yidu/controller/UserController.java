package com.yidu.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yidu.entity.Dept;
import com.yidu.server.UserService;
import com.yidu.server.impl.UserServiceImpl;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping("/login")
	public String login(@RequestParam Map<String,String> loginMap, Model model){
		System.out.println(loginMap.get("username") +":"+ loginMap.get("password"));
		
		List<Dept> deptList = userService.queryDepts();
		model.addAttribute(deptList);
		
		for (Dept dept : deptList) {
			System.out.println(dept.toString());
		}
		
		return "success";
	}
	
}
