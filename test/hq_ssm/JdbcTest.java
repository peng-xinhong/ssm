package hq_ssm;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JdbcTest {

	static ApplicationContext ctx = null;
	
	@Before
	public void setUp() throws Exception {
		ctx = new ClassPathXmlApplicationContext("spring-context.xml");
	}

	@Test
	public void test() throws SQLException {
		DataSource ds = ctx.getBean("dataSource",DataSource.class);
		System.out.println(ds.getConnection());
	}

}
